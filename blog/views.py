from django.shortcuts import render, get_object_or_404
from django.template.loader import get_template
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone
# Create your views here.
from .models import Post
from .models import About_us
from .models import Glavnaya
from .models import Contacts


from slider.models import Document
from slider.forms import DocumentForm


from slider.models import Document2
from slider.forms import DocumentForm2


from slider.models import Document3
from slider.forms import DocumentForm3





def index(request):
    return render(request, 'blog/index.html', {})


def glavnaya(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        form2 = DocumentForm2(request.POST, request.FILES)
        form2 = DocumentForm3(request.POST, request.FILES)


        if form.is_valid() and form2.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.save()


            newdoc2 = Document2(docfile2=request.FILES['docfile2'])
            newdoc2.save()

            newdoc3 = Document2(docfile2=request.FILES['docfile2'])
            newdoc3.save()


            # Redirect to the document list after POST
            return redirect('/')
    else:
        form = DocumentForm()  # A empty, unbound form
        form2 = DocumentForm()  # A empty, unbound form
        form3 = DocumentForm()  # A empty, unbound form        
    # Load documents for the list page
    documents = Document.objects.all()
    documents2 = Document2.objects.all()
    documents3 = Document3.objects.all()
    glav = get_object_or_404(Glavnaya)
    return render(request, 'blog/glavnaya.html', {'glav': glav,'documents': documents, 'documents2': documents2, 'documents3': documents3})




def about_us(request):
    about = get_object_or_404(About_us)
    return render(request, 'blog/about_us.html', {'about': about})




def contacts(request):
    cont = get_object_or_404(Contacts)
    return render(request, 'blog/contacts.html', {'cont': cont})






def news(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/news_list.html', {'posts': posts})

def news_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/news_detail.html', {'post': post})
